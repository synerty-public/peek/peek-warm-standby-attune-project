



[![Docs](https://img.shields.io/badge/docs-latest-brightgreen.svg)](http://doc.servertribe.com)
[![Discord](https://img.shields.io/discord/844971127703994369)](http://discord.servertribe.com)
[![Docs](https://img.shields.io/badge/videos-watch-brightgreen.svg)](https://www.youtube.com/@servertribe)
[![Generic badge](https://img.shields.io/badge/download-latest-brightgreen.svg)](https://www.servertribe.com/community-edition/)

# Peek Warm Standby Attune Project






# Attune

[Attune](https://www.servertribe.com/)
automates and orchestrates processes to streamline deployments, scaling,
migrations, and management of your systems. The Attune platform is building a
community of sharable automated and orchestrated processes.

You can leverage the publicly available orchestrated blueprints to increase
your productivity, and accelerate the delivery of your projects. You can
open-source your own work and improve existing community orchestrated projects.

## Get Started with Attune, Download NOW!

The **Attune Community Edition** can be
[downloaded](https://www.servertribe.com/comunity-edition/)
for free from our
[ServerTribe website](https://www.servertribe.com/comunity-edition/).
You can learn more about Attune through
[ServerTribe's YouTube Channel](https://www.youtube.com/@servertribe).







# Clone this Project

To clone this project into your own instance of Attune, follow the
[Clone a GIT Project How To Instructions](https://servertribe-attune.readthedocs.io/en/latest/howto/design_workspace/clone_project.html).




## Blueprints

This Project contains the following Blueprints.



### Add pg_wal Logical Volume ESXI


### Add pg_wal Logical Volume oVirt


### APLV Setup pg_wal Generic


### Check Database Persisted After Failover


### Check Master SSHFS Mount from Standby


### Create Database to Test Failover


### Delete Test Failover Database

Test comment

### Peek v3 LIN Test Peek Deployment


### SDTTF Create Test Failover Database on Master


### Setup Peek Server as Standby


### Setup Peek SSHFS Mount


### SPSAS Start Peek


### SPSAS Test Log Shipping


### Setup  Peek Server as Master

Sets up the master node with a new `postgresql.conf` file.




## Parameters


| Name | Type | Script Reference | Comment |
| ---- | ---- | ---------------- | ------- |
| Attune OS Build Server | Linux/Unix Node | `attuneosbuildserver` | This variable is used in the "Kickstart" build procedures, so the "Attune Server" can be used to build Attune servers. |
| KS Linux: Disk First Letter | Text | `kslinuxdiskfirstletter` | The first letter of the disk in Linux, EG, sda or xda |
| Linux: Attune User | Linux/Unix Credential | `linuxattuneuser` |  |
| Linux Master: Peek User | Linux/Unix Credential | `linuxmasterpeekuser` | The Peek user on the master PostgreSQL server. |
| Linux: Peek User | Linux/Unix Credential | `linuxpeekuser` |  |
| Linux: Root User | Linux/Unix Credential | `linuxrootuser` |  |
| Linux Standby: Peek User | Linux/Unix Credential | `linuxstandbypeekuser` | The Peek user on the standby PostgreSQL server. |
| Log Shipping Test Database | Text | `logshippingtestdatabase` | Will be created for testing log shipping and deleted afterwards. Don't use any exisiting database. |
| oVirt: Disk Interface | Text | `ovirtdiskinterface` | SATA or IDE required for Windows<br>VIRTIO_SCSI for windows after driver install<br>VIRTIO for Linux |
| oVirt: Disk Storage Name | Text | `ovirtdiskstoragename` |  |
| oVirt: Engine API User | Basic Credential | `ovirtengineapiuser` |  |
| oVirt: Engine Server | Basic Node | `ovirtengineserver` |  |
| Peek: Enable Agent | Text | `peekenableagent` |  |
| Peek: Enable Field | Text | `peekenablefield` |  |
| Peek: Enable Logic | Text | `peekenablelogic` |  |
| Peek: Enable Office | Text | `peekenableoffice` |  |
| Peek: Enable Worker | Text | `peekenableworker` |  |
| Peek Master Server | Linux/Unix Node | `peekmasterserver` | Master PostgreSQL server. |
| Peek Server | Linux/Unix Node | `peekserver` | The server where the Peek software is to be installed on. |
| Peek Standby Server | Linux/Unix Node | `peekstandbyserver` | Standby PostgreSQL server. |
| PostGreSQL archive timeout | Text | `postgresqlarchivetimeout` | postgresql.conf archive_timeout. Timeout in seconds before Write-Ahead Log file is written to the archive directory even if we don't have "wal_buffers" worth of data. |
| PostGreSQL: Directory | Text | `postgresqldirectory` |  |
| PostGreSQL: wal_buffers | Text | `postgresqlwal_buffers` | postgresql.conf archive_timeout. Write-Ahead Log buffers. Size of Write-Ahead Log file before it is written to the archive directory.<br>Add the suffix kB for kilobytes (for example 32kB).<br>Add the suffix MB for megabytes (for example 100MB). |
| Target Server | Basic Node | `targetserver` |  |
| Target Server Lin | Linux/Unix Node | `targetserverlin` | The target server is a generic placeholder, usually used for the server a script will run on.<br>For example, the server being built if the procedure is building a server. |
| Test Failover Database  | Text | `testfailoverdatabase` | Will be created for testing if data written to the master persists on the standby after the master fails. Please don't use any exisiting database. |




## Files

| Name | Type | Comment |
| ---- | ---- | ------- |
| Log Shipping postgresql.conf | Version Controlled Files |  |






# Contribute to this Project

**The collective power of a community of talented individuals working in
concert delivers not only more ideas, but quicker development and
troubleshooting when issues arise.**

If you’d like to contribute and help improve these projects, please fork our
repository, commit your changes in Attune, push you changes, and create a
pull request.

<img src="https://www.servertribe.com/wp-content/uploads/2023/02/Attune-pull-request-01.png" alt="pull request"/>

---

Please feel free to raise any issues or questions you have.

<img src="https://www.servertribe.com/wp-content/uploads/2023/02/Attune-get-help-02.png" alt="create an issue"/>


---

**Thank you**
