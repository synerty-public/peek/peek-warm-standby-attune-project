FOUND=true
TRY=1

echo "postgresqlArchiveTimeout = {postgresqlArchiveTimeout}"

# Check every 5 seconds.
# Timeout after 10 times postgresqlArchiveTimeout.
# Number of checks = 10 x postgresqlArchiveTimeout / 5

SLEEP_SECONDS=5
NUMBER_OF_CHECKS=$((10*{postgresqlArchiveTimeout}/SLEEP_SECONDS))

let "MAX_MINUTES = ${NUMBER_OF_CHECKS} * ${SLEEP_SECONDS} / 60"
echo "NUMBER_OF_CHECKS=${NUMBER_OF_CHECKS} (${MAX_MINUTES} minutes)"
echo "Checking every ${SLEEP_SECONDS} seconds."

let "EXPECTED_TRIES = {postgresqlArchiveTimeout} / ${SLEEP_SECONDS}"
echo "Expect end in check number ${EXPECTED_TRIES} ({postgresqlArchiveTimeout} seconds)."

while [ ${FOUND} = true ]
do

    if psql --list --quiet --tuples-only \
        | cut --delimiter \| --fields 1 \
        | grep --quiet --word-regexp "{logShippingTestDatabase}";
    then
        # Database exists
        # $? is 0
        
        echo "${TRY}. Waiting."
        
        if [ ${TRY} -gt ${NUMBER_OF_CHECKS} ]
        then
            echo "timed out"
            exit 1
        fi
        
        ((TRY=TRY+1))
        sleep ${SLEEP_SECONDS}
    
    else
        # Database does not exist.
        # $? is 1
        
        FOUND=false
        echo "Database dropped on standby."
    fi
    
done