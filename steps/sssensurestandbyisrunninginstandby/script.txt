cd "{postgresqlDirectory}/log/"

TODAYS_LOG="postgresql-`date +'%a'`.log"
echo "TODAYS_LOG=${TODAYS_LOG}"

if cat "${TODAYS_LOG}" \
    | grep -q 'entering standby mode'
then
    echo "Success, Standby {peekStandbyServer.hostname} did enter standby mode."
else
    echo "FAILED, Standby {peekStandbyServer.hostname} did not enter standby mode." >&2
    false
fi