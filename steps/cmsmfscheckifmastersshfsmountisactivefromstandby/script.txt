checkIfMounted () {
    FILESYSTEM="$1"
    if df -h | grep -iq "${FILESYSTEM}"
    then
        echo "Filesystem ${FILESYSTEM} is active from standby - {peekStandbyServer.hostname}"
    else
        echo "Filesystem ${FILESYSTEM} is not active from standby - {peekStandbyServer.hostname}"
       
        exit 1
    fi
}

FILESYSTEM="{linuxMasterPeekUser.user}@{peekMasterServer.ip}:/pgwal_archive/{peekMasterServer.hostname}"

if df -h | grep -iq "${FILESYSTEM}"
then
    echo "Filesystem ${FILESYSTEM} is active from standby - {peekStandbyServer.hostname}"
else
    echo "Filesystem ${FILESYSTEM} is not active from standby - {peekStandbyServer.hostname}. Trying to mount."
    
    umount /pgwal_archive/{peekMasterServer.hostname}

    rm -rf /pgwal_archive/{peekMasterServer.hostname}

    mount /pgwal_archive/{peekMasterServer.hostname}
    
    checkIfMounted "${FILESYSTEM}"
fi