pwsh <<'EOF'
$ErrorActionPreference = "Stop"
Import-Module VMware.VimAutomation.Core

Connect-VIServer {vmwareVcenterServer.ip} `
    -User "{vmwareVcenterUser.user}" `
    -Password '{vmwareVcenterUser.password}'

if ($? -eq $false) {
    Write-Host "Error: Not connected."
    exit 1
}

$vm = Get-VM "{targetServer.fqn}"

# Add 100gb of disks
$vm | New-HardDisk -CapacityGB 100 -StorageFormat Thin

EOF