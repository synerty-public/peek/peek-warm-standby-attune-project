PEEK_SERVICES=( \
    "peek_agent"
    "peek_field" \
    "peek_logic" \
    "peek_office" \
    "peek_worker"
)

for SERVICE in ${PEEK_SERVICES[@]}
do
    if systemctl list-units | grep -i "${SERVICE}"
    then
        systemctl is-active "${SERVICE}"
    else
        echo "${SERVICE} does not exist, skipping"
    fi
done