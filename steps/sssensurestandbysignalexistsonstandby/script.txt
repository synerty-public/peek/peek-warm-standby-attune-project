cd {postgresqlDirectory}
FILE="standby.signal"
if [ -f "${FILE}" ]
then
    echo "Ok, ${FILE} exists."
else
    touch ${FILE}
    echo "${FILE} does not exist. Created it"
fi